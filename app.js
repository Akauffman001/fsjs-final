const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
var mongoose = require('mongoose');
var User = require('./models/user');
var List = require('./models/list');
var session = require('express-session');
var mongoStore = require('connect-mongo')(session);
var mid = require('./middleware');

const app = express();
app.use(bodyParser.urlencoded({extended: false}));
//cookie parser middleware
app.use(cookieParser());
//switch public path name
app.use('/static', express.static('./public'));
//expect pug files in view folder
app.set('view engine', 'pug');

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//mongodb connection
mongoose.connect("mongodb://localhost:27017/flkrlst", { useNewUrlParser: true, useCreateIndex: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:', ));

//use session to track logins
app.use(session({
    secret: 'flkrlst',
    resave: true,
    saveUnitalized: false,
     store: new mongoStore({
        mongooseConnection: db
    })
}));

//make user id avail to templets
app.use(function(req,res,next){
    res.locals.currentUser = req.session.userId;
    next();
});


/*******************************************************
                        routes
/******************************************************/
//home get /
app.get('/',(req,res)=>{
    if(typeof req.cookies.email === "undefined"){
     res.render('index', {title: 'logged out', treehousename: "", name: ""});
    }else{
     res.render('index', {title: 'Home'});
    }
        });
//flkr lst get /flickerlist
app.get('/flickrList',(req,res)=>{
     res.render('list', {title: 'Flikr List'});
        });
//FUTURE route will connect to db (work in progress)
app.post('/flickrList',(req,res)=>{
     // create object with form input
      var listData = {
        li: req.body.item
      };
    // use schema's `create` method to insert document into Mongo
      List.create(listData, function (error, list) {
        if (error) {
          return next(error);
        } else {
          return res.render('list');
        }
      });
        });

app.get('/register', mid.loggedOut, (req,res)=>{
     res.render('register', {title: 'Register'});
        });

app.post('/register',(req,res, next)=>{
       if (req.body.email &&
    req.body.name &&
    req.body.quote &&
    req.body.password &&
    req.body.confirmPassword) {

      // confirm that user typed same password twice
      if (req.body.password !== req.body.confirmPassword) {
        var err = new Error('Passwords do not match.');
        err.status = 400;
        return next(err);
      }

      // create object with form input
      var userData = {
        email: req.body.email,
        name: req.body.name,
        quote: req.body.quote,
        treehouse: req.body.treehouse,
        password: req.body.password
      };

      // use schema's `create` method to insert document into Mongo
      User.create(userData, function (error, user) {
        if (error) {
          return next(error);
        } else {
          res.cookie('email', req.body.email);       //trying cookies.
          res.cookie('password', req.body.password); //trying cookies.
          req.session.userId = user._id;
          return res.redirect('/profile');
        }
      });

    } else {
      var err = new Error('All fields required.');
      err.status = 400;
      return next(err);
    }
        });
//update profile information POST /update
app.post('/update',(req,res, next)=>{
    //compare entered info == signed in info
       if (req.body.email == req.cookies.email &&
    req.body.name &&
    req.body.quote &&
    req.body.password == req.cookies.password &&
    req.body.confirmPassword == req.cookies.password) {

      // confirm that user typed same password twice
      if (req.body.password !== req.body.confirmPassword) {
        var err = new Error('Passwords do not match.');
        err.status = 400;
        return next(err);
      }

      // update object with form input
       userData = {
        email: req.body.email,
        name: req.body.name,
        quote: req.body.quote,
        treehouse: req.body.treehouse,
        password: req.body.password
      };
    if(userData.treehouse == ""){
        //find/update document in Mongo
      User.findOneAndUpdate({"email": userData.email}, {$set: {"name":userData.name,
       "quote":userData.quote                                 
        }}, {upsert:true}, function (error, user) {
        if (error) {
          return next(error);
        } else {
          req.session.userId = user._id;
          return res.redirect('/profile');
        }
      });
    }else{
      //find/update document in Mongo
      User.findOneAndUpdate({"email": userData.email}, {$set: {"name":userData.name,
       "quote":userData.quote, "treehouse":userData.treehouse                
        }}, {upsert:true}, function (error, user) {
        if (error) {
          return next(error);
        } else {
          req.session.userId = user._id;
          return res.redirect('/profile');
        }
      });
    }
    } else {
      var err = new Error('Please enter correct user name and password.');
      err.status = 400;
      return next(err);
    }
        });

app.post('/delete',(req,res, next)=>{
       if (req.body.email == req.cookies.email) {
      // find email entered in db and delete
      User.findOneAndDelete({"email": req.body.email}, function (error) {
                if (error) {
                  return next(error);
                    } else {
                        if (req.session) {
                        // delete session object
                        req.session.destroy(function(err) {
              if(error) {
                return next(error);
              } else {
                res.clearCookie('email'); //clear browser info of deleted user
                res.clearCookie('password');//clear browser info of deleted user
                return res.redirect('/');
              }
            });
          }
        }
      });

    } else {
      var err = new Error('Please enter the email of the account you are currently logged in to');
      err.status = 400;
      return next(err);
    }
        });

// GET /logout
app.get('/logout', function(req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function(err) {
      if(err) {
        return next(err);
      } else {
        res.clearCookie('email');
        res.clearCookie('password');
        return res.redirect('/');
      }
    });
  }
});

app.get('/login', mid.loggedOut, (req,res)=>{
     res.render('login', {title: 'login'});
        });

app.post('/login',(req,res, next)=>{
    if (req.body.password && req.body.email) {
      User.authenticate(req.body.email, req.body.password, function(error, user){
          if (error || !user){
            var err = new Error('Incorrect password, please try again');
            err.status = 401;
            next(err);
          } else {
              res.cookie('email', req.body.email);       //trying cookies. refer to README.md
              res.cookie('password', req.body.password); //trying cookies.
              req.session.userId = user._id;
              return res.redirect('/profile');
          }
      }); //method created in models
      }else{
          var err = new Error('fill in all fields');
          err.status=401;
          return next(err);
      }
        });

app.get('/profile', mid.requiresLogin, function(req, res, next) {
  if (! req.session.userId ) {
    var err = new Error("You are not authorized to view this page.Please login to view profile information");
    err.status = 403;
    return next(err);
  }
  User.findById(req.session.userId)
      .exec(function (error, user) {
        if (error) {
          return next(error);
        } else {
          return res.render('profile', { title: 'Profile', name: user.name, quote: user.quote, treehousename: user.treehouse });
        }
      });
});

//handles misspelt routes
app.all('*', function(req, res) {
  res.redirect("/");
});

// error handler
// define as the last app.use callback
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    title: 'Error',
    message: err.message,
    status: err.status,
    stack: err.stack,
    error: {}
  });
});
app.listen(3000, ()=>
          console.log('the application is running on localhost:3000')
          );