//midware to restrict certian routes for logged in users
function loggedOut(req, res, next) {
  if (req.session && req.session.userId) { 
    return res.redirect('/profile');//send current session holders to profiles
  }
  return next();
}
//midware to restrict certian routes for logged out users
function requiresLogin(req, res, next){
    if(req.session && req.session.userId){
        return next();
    }else{
        var err = new Error('you must be logged in to view this page');
        err.status = 401;
        return next(err);
    }
}

module.exports.loggedOut = loggedOut;
module.exports.requiresLogin = requiresLogin;