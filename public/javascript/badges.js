console.log(name);

var info = name.split("<<->>");

if(name === "<<->><<->>"){
$.get("https://teamtreehouse.com/adam5808.json", function(response){
      var galleryItem = ""; 
    //dynamically create gallery items
    for (let i = 0; i < response.badges.length; i++){
       galleryItem = `<li><img src="${response.badges[i].icon_url}"><p>${response.badges[i].name}</p></li>`;
        $('#gallery').last().append(galleryItem);
          }
    //dynamically create treehouse info card
    $('#profile-info').append(`  
  <h6>Treehouse Profile</h6>
  <img src="${response.gravatar_url}" alt="${response.name}">
  <h1>${response.name}</h1>
  <p id="profile-name">Bellarmine Univerisy</p>
  <p><a href="${response.profile_url}">${response.profile_name}</a></p>
    <ul id="points-left"></ul>
    <ul id="points-right"></ul>

  <p><a href="${response.profile_url}"><button id="profile-button">View</button></a></p>`);
    let i = 0; 
    $.each(response.points, function(key, value){ //retrieve points and category
        let liPoints = `<li><span>${value}</span><p>${key}</p></li>`;
        if (value > 0){
            if(i % 2 == 0){  //to evenly distribute points and categories
                $("#points-left").append(liPoints);
            } else{
                $("#points-right").append(liPoints);
            }
                 i += 1;
        }
    });
});
}else{
    document.getElementById("logo").firstElementChild.innerText = info[0]
    var url = "https://teamtreehouse.com/";
    url += info[1];
    url += ".json";
    $.get(url, function(response){
      var galleryItem = ""; 
    //dynamically create gallery items
    for (let i = 0; i < response.badges.length; i++){
       galleryItem = `<li><img src="${response.badges[i].icon_url}"><p>${response.badges[i].name}</p></li>`;
        $('#gallery').last().append(galleryItem);
          }
    //dynamically create treehouse info card
    $('#profile-info').append(`  
  <h6>Treehouse Profile</h6>
  <img src="${response.gravatar_url}" alt="${response.name}">
  <h1>${response.name}</h1>
  <p id="profile-name">${info[2]}</p>
  <p><a href="${response.profile_url}">${response.profile_name}</a></p>
    <ul id="points-left"></ul>
    <ul id="points-right"></ul>

  <p><a href="${response.profile_url}"><button id="profile-button">View</button></a></p>`);
    let i = 0; 
    $.each(response.points, function(key, value){ //retrieve points and category
        let liPoints = `<li><span>${value}</span><p>${key}</p></li>`;
        if (value > 0){
            if(i % 2 == 0){  //to evenly distribute points and categories
                $("#points-left").append(liPoints);
            } else{
                $("#points-right").append(liPoints);
            }
                 i += 1;
        }
    });
}).fail(function(){
       $('#gallery').append(`<p>ERROR: Couldnt find treehouse account name. please double check the spelling of ${info[1]}. To update your information with a valid treehouse username you must navigate to My Profile page.</p>`)
    });
}