if(title == 'Flikr List'){ //to eliminate console errors for pages that dont req js
const toggleList = document.querySelector('#toggleList');
const listDiv = document.querySelector('.list');
const descriptionInput = document.querySelector('input.description');
const descriptionP = document.querySelector('p.description');
const descriptionButton = document.querySelector('button.description');
const listUl = document.querySelector('#crud-list');
const addItemInput = document.querySelector('input.addItemInput');
const addItemButton = document.querySelector('button.addItemButton');
const lis = listUl.children;
const firstli = listUl.firstElementChild;
const lastli = listUl.lastElementChild;
firstli.style.backgroundColor = 'grey';
lastli.style.backgroundColor = '#999';
 var flickerAPI = "https://api.Flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
$('#flashMessage').hide(); //hide FLICKR search message

callupdatebtn(); //add inital buttons and colors


$('.additem').on('submit', function (event) { //FUTURE FUNCTION
event.preventDefault(); // Stop the form from causing a page refresh.
    var lstInput = listUl.lastElementChild.textContent.replace('DOWN', '').replace('UP', '').replace('REMOVE', ''); 
    var data ={
        item: lstInput
    };
    
//    $.ajax({
//      url: 'http://localhost/flickrList',
//      data: data,
//      method: 'POST'
//    }).then(function (response) {
//     
//    }).catch(function (err) {
//      console.error(err);
//    });
});
/*****************************************************************************
CRUD LIST METHODS:
button management- callremovebtn, removebtn, callupdatebtn, updatebtn, annyclk
list toggle- annyclk
add item- annyclk (anonymous method in click event)
***************************************************************************
================REMOVE BUTTONS FROM CRUD LIST==================
*/
//calls method to remove button across all list items
function callremovebtn(){
for(let i = 0; i < lis.length; i+=1){
removebtn(lis[i]);
}}

//removes buttons from list item
function removebtn(li){
    let buttonCount = li.childElementCount;
    var count = 0;
 if(buttonCount === 3){
    count = 0;
   while(count <= 2){
   li.removeChild(li.firstElementChild);
    count++;
}
 } else if(buttonCount === 2){
    count = 0;
   while(count <= 1){
   li.removeChild(li.firstElementChild);
    count++;
}}}

//adds buttons to list items respectfully
function callupdatebtn(){
for(let i = 0; i < lis.length; i+=1){
updatebtn(lis[i]);
}}
/*
================RECREATE/ATTACH BUTTONS==================
*/
//adds certian buttons to list items depending on postition, also colors.
function updatebtn(li){
    if(li.childElementCount == 0){  
        if(listUl.lastElementChild === listUl.firstElementChild){
           
           } else if (li === listUl.lastElementChild){             
            console.log();
            let up = document.createElement('button');
            up.className = 'up';
            up.textContent = ' UP';
            li.appendChild(up);
            li.style.backgroundColor = '#999';
        } else  if(li === listUl.firstElementChild){ 
            let down = document.createElement('button');
            down.className = 'down';
            down.textContent = ' DOWN';
            li.appendChild(down);
            li.style.backgroundColor = 'grey';
        } else {
            up = document.createElement('button');
            up.className = 'up';
            up.textContent = ' UP';
            li.appendChild(up);

            down = document.createElement('button');
            down.className = 'down';
            down.textContent = 'DOWN';
            li.appendChild(down);
            li.style.backgroundColor = "white";

            }
            remove = document.createElement('button'); 
            remove.className = 'remove';
            remove.textContent = 'REMOVE';
            li.appendChild(remove);           
}
  if( listUl.childElementCount === 2 && listUl.firstElementChild.childElementCount === 1){
             down = document.createElement('button');
            down.className = 'down';
            down.textContent = 'DOWN';
            li.parentElement.firstElementChild.appendChild(down);
            li.style.backgroundColor = "white";
               }
}

//button functionality for list item up, down, remove
listUl.addEventListener('click', (event) => {
      let li = event.target.parentNode;
      let prevLi = li.previousElementSibling;
      let ul = li.parentNode;
  if (event.target.tagName == 'BUTTON') { //if clicks a button
    //   $('#selected').remove();
    if (event.target.className == 'remove') { //if the button was remove
      ul.removeChild(li); //remove the list item
    }
    if (event.target.className == 'up') {
      if (prevLi) { //if it has a list item before it, else do nothing
        ul.insertBefore(li, prevLi); //swap position with previous element
      }   
    } 
    if (event.target.className == 'down') {
      let li = event.target.parentNode;
      let nextLi = li.nextElementSibling;
      let ul = li.parentNode;
        if (nextLi) {
        ul.insertBefore(nextLi, li);
        }
  }
} 
 callremovebtn();
 callupdatebtn();
});
/*
================HIDE LIST TOGGLE==================
*/
toggleList.addEventListener('click', () => {
      $('#temp-gallery ul').remove();
  if (listDiv.style.display == 'none') {
    toggleList.textContent = 'Hide list';
    listDiv.style.display = 'block';
  } else {
    toggleList.textContent = 'Show list';                        
    listDiv.style.display = 'none';
  }                         
});

descriptionButton.addEventListener('click', () => {
  descriptionP.innerHTML = descriptionInput.value + ':';
  descriptionInput.value = '';
});
/*
================ADD CRUD LIST==================
*/
addItemButton.addEventListener('click', () => {
  let ul = document.querySelector('#crud-list');
  let li = document.createElement('li');
  li.textContent = addItemInput.value;
  ul.appendChild(li);
  updatebtn(li);
  callremovebtn();
  callupdatebtn();
  addItemInput.value = '';
});

/*****************************************************************************
CRUD LIST JQUERY METHODS:
flikr gallery- annyclk, anny get function, anny each function
****************************************************************************
================FLICKR GALLERY DISPLAY==================
*/
   $(document.body).on('click', '#crud-list li', function(){
   var textclick = $(this).text().replace('DOWN', '').replace('UP', '').replace('REMOVE', ''); 
   $('#temp-gallery ul').remove();
   $('#flashError').attr('id', 'flashMessage').text('Searching...');
   $('#flashMessage').slideDown();

//gets flicker api related to clicked item
$.getJSON(flickerAPI, {
        tags: textclick,
        format: "json"
      },
    function(data){
    console.log(data);
    var photoHTML = '<ul>';
      if (data.items.length > 0) {
        $.each(data.items,function(i,photo) {
          photoHTML += '<li>';
          photoHTML += '<a href="' + photo.link + '" class="image">';
          photoHTML += '<img src="' + photo.media.m + '"></a></li>';
        }); // end each
      } else {
        photoHTML = "<p>No photos found that match: " + textclick + ".</p>"
      }
      photoHTML += "</ul>";
      $('#temp-gallery p').remove();
      $('#temp-gallery').append(photoHTML);
      $('#flashMessage').slideUp();
    }).fail(function (jqXHR){ //IF error with ajax
    $('#flashMessage').attr('id', 'flashError').text(`FLICKR ${jqXHR.status} ${jqXHR.statusText}: please try again in a little.`);
   $('#flashError').slideUp();
}); // end getJSON
});
}
