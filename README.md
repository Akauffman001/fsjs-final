# Adam Kauffman's FSJS project
## About
I chose to rebuild my frontend project to experience the difference between a static and dynamic site first hand. Using the power of fullstack javascript, i allowed users to register to my site storing their information in mongodb. Once logged in, they can view their information and update/delete their accounts in the database collection. the site also includes a CRUD LIST which is connected to a flicker API to display an image gallery for any list item added. My home page shows off my credentials by displaying my treehouse badges and info dynamically with treehouse's free API.

## Installation
navigate to your projects directory using your terminal
run 'npm install' to download the projects dependencies
this should create the node_modules folder


```requires
    npm
```

```command
npm install
```
## MongoDB Service 
Start MongoDB's service before the server: Navigate to the bin directory in the mongodb program folder (naturally installed under Program Files on your primary hard drive). Once in the bin directory run the command 'mongod' which starts the service. In order to access mongo's shell you will need to open another console. navigate to the bin directory, as before, and run the command 'mongo'.

```requires
    MongoDB
```

```command
mongod
mongo
```

## Usage
start the server. The entry point is app.js. i chose to have my middleware and routes in one files, seperated by comments. To start the server, navigate to the project's folder in terminal and run 'node app.js' or 'npm start'. Optionally, you can use nodemon if you use npm to install it. the server automatically is on port 3000, navigate to your local host to view the web application. 

```command
'npm start' || 'node app.js'
optionally: 'nodemon'
```
## Contributing and extra info
Pull requests are welcome. This website focuses on dom manipulation, express server and routing, templating in pug, basic storage/update/delete from mongodb  however, please keep in mind this webapp is still in development. Please note, It is not designed to house REAL sensitive information. To delete/update information from the database it compares user inputs to the username and password in cookies. This purely was to practice material such as cookies and the package cookie parser. I wanted to use all three state storage techniques in my web app. using a cookie in this way is not standard practice and such info should be stored in a more secure way. It can be switched if intended for large scale use.



## License
[MIT](https://choosealicense.com/licenses/mit/)